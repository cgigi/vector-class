#include <cassert>
#include <iostream>

#include "../src/vector3.hh"


int main()
{

    std::cout << "test addition" << std::endl;
    auto v1 = vector::Vector3(-1.9, 0.8, 15);
    auto v2 = vector::Vector3(10.009, 8.1, -3);


    auto ref = vector::Vector3(10.009 - 1.9, 0.8 + 8.1, 15 - 3);

    assert(((v1 + v2) == ref) == true);

    return 0;

}
